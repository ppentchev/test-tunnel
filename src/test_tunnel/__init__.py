# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause
"""Top-level definitions for the tunnel testing library."""

from __future__ import annotations

import typing


if typing.TYPE_CHECKING:
    from typing import Final


VERSION: Final = "0.1.2"
