<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# Changelog

All notable changes to the test-tunnel project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2025-02-13

### Fixes

- Recreate the client socket on connection failure; on some operating systems
  a failed `connect(2)` call will leave the socket in an inconsistent state.
- Correct the license clause in `pyproject.toml`.
- Bump the build dependency on `hatchling` to 1.26 for PEP 639 licensing support.

### Additions

- Point to the `README.md` file in `pyproject.toml`.
- List Python 3.13 as a supported version.
- Tentatively list Python 3.14 as a supported version.
- Test suite:
    - add a manually-invoked `pyupgrade` Tox environment

### Other changes

- Switch to PEP 639 licensing.
- Test suite:
    - use Ruff 0.9.6 and override one more docstring check

## [0.1.1] - 2024-08-06

### Additions

- Documentation:
    - adapt the documentation index to add a `README.md` file

## [0.1.0] - 2024-08-06

### Started

- First public release.

[Unreleased]: https://gitlab.com/ppentchev/test-tunnel/-/compare/release%2F0.1.2...main
[0.1.2]: https://gitlab.com/ppentchev/test-tunnel/-/compare/release%2F0.1.1...release%2F0.1.2
[0.1.1]: https://gitlab.com/ppentchev/test-tunnel/-/compare/release%2F0.1.0...release%2F0.1.1
[0.1.0]: https://gitlab.com/ppentchev/test-tunnel/-/tags/release%2F0.1.0
