<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# API Reference

## The test-tunnel library

- [`test_tunnel.defs`: the common definitions module](defs.md)
- [`test_tunnel.addresses`: the network interface address discovery module](addresses.md)
- [`test_tunnel.run_test`: the test runner infrastructure](run_test.md)
- [`test_tunnel.cmd_test`: the example command-line testing tools](cmd_test.md)
