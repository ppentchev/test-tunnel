<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# The test-tunnel sample test tools

::: test_tunnel.cmd_test
    options:
      members: []

## Test the `microsocks` simple SOCKS5 server implementation

::: test_tunnel.cmd_test.microsocks

## Test the `socat` tool's TCP connection forwarding

::: test_tunnel.cmd_test.socat
