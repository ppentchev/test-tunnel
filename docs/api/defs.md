<!--
SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
SPDX-License-Identifier: BSD-2-Clause
-->

# The test-tunnel common definitions module

::: test_tunnel.defs
    options:
      members: []

## Configuration classes

::: test_tunnel.defs.Config

::: test_tunnel.defs.ConfigProg
